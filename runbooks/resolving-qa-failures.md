# Overview

This document describes the process to follow if QA tests are failing.

QA smoke tests are run as part of the auto-deploy pipeline - this means they are run regularly and can be assumed as stable.


# Process Overview

Failing QA tests are always treated as an S2 incident - this helps the blocker receive the correct attention and gives us an audit trail. Quality are here to help us investigate and resolve any failures and maintain a schedule to show who to ping. All test results are posted in the #qa-<env> Slack channels.

# Process steps

1. When QA tests fail open an S2 incident
1. Check the `qa-<env>` Slack channel, the failure may already be known and being worked on
1. Decide whether to retry the test, or escalate - if you're not sure always escalate
1. To escalate simply ping the engineer listed in the [Quality on call schedule] and ask for assistance. The #quality Slack channel can also be used.  
1. Once tests are passing again update the incident issue to indicate the cause of failure (software-change, config-change, flaky-test)

# Quality on call  

* Quality maintain a schedule of engineers on call to assist us. See [their responsibilities](https://about.gitlab.com/handbook/engineering/quality/guidelines/#responsibility).

* [Quality on call schedule]

[Quality on call schedule]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/oncall-rotation/#schedule
